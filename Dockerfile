##
## HealthVerity base NGINX image
##
## HealthVerity needs a Docker container running nginx to serve a simple html 
## document. We need to be able to use the same container in both development 
## and production modes, configured by setting an environment variable 
## ‘ENVIRON’ to ‘dev’ or ‘prod’. The server should log to stdout for access 
## logs, and stderr for error logs. In prod, the nginx config should define 50 
## worker processes, worker_rlimit_nofile of 8192, and worker_connections at 
## 4096. In dev, the nginx config should define 10 worker processes, 
## worker_rlimit_nofile of 4096, and worker_connections at 1024. Both dev and 
## prod should serve the contents of /var/www/. The listening port is not 
## important, as it can be mapped at run time. The deliverable container should 
## be hosted on hub.docker.com, unless you prefer to use another container 
## repository.
##
## Please build a self contained Docker container that excutes nginx in this
## fashion. No additional modules should be used for this task. 
##
## Bonus 1: What modules are available that would change your approach to this 
## task?
##
## Bonus 2: How would you approach remote logging in production mode to 
## stream the error and access logs to Amazon’s CloudWatch?
##
## History:
##		06.14.2016 David Pheasant	- File Created
##
###############################################################################
FROM nginx
ENV  ENVIRON	dev
COPY prod.conf  /etc/nginx/prod.conf
COPY dev.conf   /etc/nginx/dev.conf
COPY index.html /var/www/
RUN chown -R nginx:nginx /var/www
CMD echo "ENVIRON: ${ENVIRON}" && nginx -c /etc/nginx/${ENVIRON}.conf